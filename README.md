## Description

The main purpose of the application is to proxy calls to leonbets from Call Center. Call Center is hosted in an external datacenter and is not included into Leongaming networks.

Call Center is one instance application which has only production env, and we have to solves such issues:
- Due to external company manage Call Center application, we want to have some kind of possibility to change remote adress without configuration at Motion (part of Call Center) side.
- Call Center must serve clients even if Leonbets is not accessible during restarts.

### Running the server
$ git clone git@git.leoncorp.net:alexey.koptenkov/callcenter-leonbets-proxy.git
$ cd callcenter-leonbets-proxy

### Edit connection settings
$ vi leon-proxy.config.js

```
module.exports = {
  apps : [{
    name        : "leon-proxy",
    script      : "./server.js",
    watch       : true,
    env: {
      "NODE_ENV": "development",
      "SKEY": "",
      "BASE_URL": "",
      "SMTP_HOST": "mail.leongaming.com",
      "SMTP_PORT": 25,
      "SMTP_USER": "callbacks@leongaming.com>",
      "SMTP_PASS": "",
      "MAIL_FROM": '"Call Center" <no-reply@leongaming.com>',
      "MAIL_TO": '"Callback" <callbacks@leongaming.com>',
      "PREPEND_TO_SMS": 'Актуальный адрес сайта БК Леон: ',
      "TIMEOUT": 5000
    },
    env_production : {
      "NODE_ENV": "production",
      "SKEY": "",
      "BASE_URL": "",
      "SMTP_HOST": "mail.leongaming.com",
      "SMTP_PORT": 25,
      "SMTP_USER": "callbacks@leongaming.com>",
      "SMTP_PASS": "",
      "MAIL_FROM": '"Call Center" <no-reply@leongaming.com>',
      "MAIL_TO": '"Callback" <callbacks@leongaming.com>',
      "PREPEND_TO_SMS": 'Актуальный адрес сайта БК Леон: ',
      "TIMEOUT": 5000
    }
  }]
}

```

### Service management
The application is managed by [pm2](http://pm2.keymetrics.io/) which is installed as systemd service. The most common commands:

##### Display all processes status
$ pm2 list            
##### Stop specific process id
$ pm2 stop 0             
##### Restart specific process id
$ pm2 restart 0          
##### Will remove process from pm2 list
$ pm2 delete 0          

#### Run with development settings
$ pm2 start leon-proxy.config.js

#### Run with production settings
$ pm2 start leon-proxy.config.js --env production


### GET /sms

> Receive an actual leonbets domain name 

Sample of OK response

```
status: 200
body:   {"text": "http://xxxx.com"}
```

Sample of NOK response

```
status: 404
body: <empty>
```

### GET /preauth/{phone}

> Preauthorize customer by phone number

Sample of response

```
status: 200
body: {"authorized": 0}

```

- 0 - authorized     (accept)
- 1 - not authorized (reject)


### POST /auth

> Authorize customers by login

#### body

```
{
    "login": <login>
    "phone": <phone>,
    "callId": <callId>

}
```

Sample of response

```
status: 200
body:   {"authorized": 0}
```

- 0 - authorized     (accept)
- 1 - not authorized (reject)
- 2 - is not vip     (reject)
- 3 - blacklisted    (reject)
- 4 - non active     (reject)


### GET /status

> Check if service available

0 - means do not disturb, 1 - means service is available

Sample of response

```
status: 200
body:   {"enabled": 0}
```

### POST /callback

> Request a callback


#### header

```
Content-Type: application/json
```

#### body

```
{
    "login": <login>
    "phone": <phone>
}
```

Sample of OK response

```
status: 200
body: <empty>
```

Sample of NOK response

```
status: 400
body: <empty>
```

