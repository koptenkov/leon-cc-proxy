const http =require('http');
const axios = require('axios');
const express = require('express');
const terminus = require('@godaddy/terminus');
const mcache = require('memory-cache');
const nodemailer = require('nodemailer');

const app = express();

const port = process.env.PORT || 3000;
const skey = process.env.SKEY;

const smtpConfig = {
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT,
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASS
  }
};
 
const mailConfig = {
  from: process.env.MAIL_FROM,
  to:  process.env.MAIL_TO
}

const timeout = process.env.TIMEOUT;
const CancelToken = axios.CancelToken;

var cachedHost = null;

axios.defaults.baseURL = process.env.BASE_URL;
axios.defaults.timeout = timeout;
axios.defaults.headers.get['Accept'] = 'application/json';
axios.defaults.headers.post['Content-Type'] = 'application/json';

axios.defaults.headers.get['User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36';

app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.get('/status', (req, res) => {
  let source = CancelToken.source();
  setTimeout(() => {source.cancel();}, timeout);
  axios.get('/callcenter', {cancelToken: source.token})
    .then(response => {
console.info("Call Center is ", response);
      if (response.data && response.status === 200 && response.headers['content-type'] === 'application/json' && response.data.result !== 'ERROR') {
        res.json({enabled: Number(response.data.enabled) == 1 ? 0 : 1});
      } else {
        res.json({enabled: 1});
      }
    })
    .catch(error => {
console.error("An error occured while checking Call Center status ", error);
      res.json({enabled: 1});
    });
});

app.get('/preauth/:phone', (req, res) => {
  let source = CancelToken.source();
  setTimeout(() => {source.cancel();}, timeout);
  axios.get('/auth/' + req.params.phone, {cancelToken: source.token})
    .then(response => {
console.info("preauth is started ", response);
      if (response.data && response.status === 200 && response.headers['content-type'] === 'application/json' && response.data.result !== 'ERROR') {
        res.json(response.data); 
      } else {
        res.json({authorized: 1});
      }
    })
    .catch(error => {
console.error("An error occured while preauth ", error);
       res.json({authorized: 1});
    });
});

app.post('/auth', (req, res) => {
  let data = {
    login: req.body.login,
    phone: req.body.phone,
    callId: req.body.callId,
    now: new Date().getTime()
  };

  let source = CancelToken.source();
  setTimeout(() => {source.cancel();}, timeout);

  axios.post('/auth/', data, {headers: {'skey': skey}, cancelToken: source.token})
    .then(response => {
console.info("Received auth response - ", response);
      if (response.data && response.status === 200 && response.headers['content-type'] === 'application/json' && response.data.result !== 'ERROR') {
        res.json(response.data); 
      } else {
        res.json({authorized: 1});
      }
    })
    .catch(error => {
console.error("An error ocurred while received auth response - ", error);
      res.json({authorized: 1});
    });
});

app.get('/sms', (req, res) => {
  let cachedBody = mcache.get('host');

  if (cachedBody) {
    res.json({text: cachedBody});
  } else {
    let source = CancelToken.source();
    setTimeout(() => {source.cancel();}, timeout);

    axios.get('/mirror', {headers: {'skey': skey}, cancelToken: source.token})
      .then(response => {
        if (response.data && response.status === 200 && response.headers['content-type'] === 'application/json' && response.data.result !== 'ERROR') {
          cachedHost = mcache.put('host', response.data.host, 60 * 1000, (k, v) => {
            console.log(k + ' did ' + JSON.stringify(v));
          });

          res.json({text: process.env.PREPEND_TO_SMS + cachedHost});
        } else {
          if (cachedHost) {
            res.json({text: process.env.PREPEND_TO_SMS + cachedHost});
          } else {
            res.status(404).end();
          }
        }
    }).catch(error => {
      if (cachedHost) {
        res.json({text: process.env.PREPEND_TO_SMS + cachedHost});
      } else {
        res.status(404).end();
      }
    });
  }
});

app.post('/callback', (req, res) => {
  let data = {
    login: req.body.login,
    phone: req.body.phone,
    now: new Date().getTime()
  };

  let source = CancelToken.source();
  setTimeout(() => {source.cancel();}, timeout);

  axios.post('/callback', data, {headers: {'skey': skey}, cancelToken: source.token})
    .then(response => {
      if (response.status === 204) {
        res.status(200).end();
      } else {
        notify(data)
          .then((info) => {
            res.status(200).end();
          })
          .catch((err) => {
            res.status(400).end(); 
          });
      }
    }).catch(error => {
        notify(data)
          .then((info) => {
            res.status(200).end();
          })
          .catch((err) => {
            res.status(400).end(); 
          });
    });
});

function notify(data) {
  let transporter = nodemailer.createTransport(smtpConfig, mailConfig);

  // verify connection configuration
  transporter.verify((error, success) => {
    if (error) {
      console.log(error);
    } else {
      console.log('Server is ready to take our messages');
    }
  });

  return transporter.sendMail({
    subject: 'Callback requested by ' + data.login + ', at ' + new Date(data.now).toLocaleString(),
    text: 'Phone ' + data.phone
  });
};

function onSignal() {
  console.log('server is starting cleanup');
  // start cleanup of resource, like databases or file descriptors
  return Promise.all([
    // your clean logic, like closing database connections
  ]);
}

function onHealthCheck() {
  // checks if the system is healthy, like the db connection is live
  // resolves, if health, rejects if not
  return Promise.resolve();
}

function onShutdown() {
  console.log('cleanup finished, server is shutting down');
}

terminus(http.createServer(app), {
  logger: console.log,
  signal: 'SIGINT',
  healthChecks: {
    '/healthcheck': onHealthCheck,
  },
  onSignal,
  onShutdown
}).listen(port, 'localhost', () => console.log('leon-proxy app listening on port ' + port + '!'));
