module.exports = {
  apps : [{
    name        : "leon-proxy",
    script      : "./server.js",
    watch       : true,
    env: {
      "NODE_ENV": "development",
      "SKEY": "",
      "BASE_URL": "",
      "SMTP_HOST": "mail.leongaming.com",
      "SMTP_PORT": 25,
      "SMTP_USER": "callbacks@leongaming.com",
      "SMTP_PASS": "",
      "MAIL_FROM": '"Call Center" <no-reply@leongaming.com>',
      "MAIL_TO": '"Callback" <callbacks@leongaming.com>',
      "PREPEND_TO_SMS": 'Актуальный адрес сайта БК Леон: ',
      "TIMEOUT": 5000
    },
    env_production : {
      "NODE_ENV": "production",
      "SKEY": "",
      "BASE_URL": "",
      "SMTP_HOST": "mail.leongaming.com",
      "SMTP_PORT": 25,
      "SMTP_USER": "callbacks@leongaming.com",
      "SMTP_PASS": "",
      "MAIL_FROM": '"Call Center" <no-reply@leongaming.com>',
      "MAIL_TO": '"Callback" <callbacks@leongaming.com>',
      "PREPEND_TO_SMS": 'Актуальный адрес сайта БК Леон: ',
      "TIMEOUT": 5000
    }
  }]
}
